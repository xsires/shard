CREATE TABLE user_payment (
  id  VARCHAR(30),
  sum NUMERIC(19, 2)
);

CREATE TABLE shop_payment (
  id  VARCHAR(30),
  sum NUMERIC(19, 2)
);