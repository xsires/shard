package ru.xsires.shard.engine.resurrection;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.xsires.shard.config.ShardsProperty;
import ru.xsires.shard.engine.Shard;
import ru.xsires.shard.engine.ShardsHolder;
import ru.xsires.shard.engine.check.HealthCheckResult;
import ru.xsires.shard.engine.check.ShardHealthChecker;

@ExtendWith(SpringExtension.class)
@Configuration
@ContextConfiguration(classes = {ShardResurrectionImpl.class,
        ShardResurrectionTest.ShardResurrectionTestConfiguration.class})
class ShardResurrectionTest {

    @MockBean
    ShardHealthChecker shardHealthChecker;
    @Autowired
    ShardResurrection shardResurrection;

    @Autowired
    ShardsHolder shardsHolder;

    @BeforeEach
    public void init() {

    }

    @Test
    public void should_healthCheck_when_resurrect() throws Exception {
        List<Shard> shards = shardsHolder.getShards();
        Shard shard = shards.get(0);

        when(shardHealthChecker.check(shard)).thenReturn(HealthCheckResult.FAIL);

        shardResurrection.resurrect(shard);

        Mockito.verify(shard).setShardStatus(Shard.ShardStatus.BROKEN);
        Thread.sleep(200);
        Mockito.verify(shardHealthChecker, VerificationModeFactory.atLeast(1)).check(shard);
        when(shardHealthChecker.check(shard)).thenReturn(HealthCheckResult.SUCCESS);
        Thread.sleep(200);
        Mockito.verify(shard, VerificationModeFactory.atLeast(1))
                .setShardStatus(Shard.ShardStatus.ACTIVE);
    }

    @TestConfiguration
    static class ShardResurrectionTestConfiguration {

        @Bean
        public ShardsProperty shardsProperty() {
            ShardsProperty mock = mock(ShardsProperty.class);
            when(mock.getResurrectionDelay()).thenReturn(Duration.ofMillis(100));
            return mock;
        }

        @Bean
        public ShardsHolder shardsHolder() {
            List<Shard> shards = Arrays.asList(Mockito.mock(Shard.class),
                    Mockito.mock(Shard.class),
                    Mockito.mock(Shard.class));
            ShardsHolder shardsHolder = Mockito.mock(ShardsHolder.class);
            when(shardsHolder.getShards()).thenReturn(shards);
            for (byte i = 0; i < 3; i++) {
                Shard shard = shards.get(i);
                when(shard.getShardStatus()).thenReturn(Shard.ShardStatus.ACTIVE);
                when(shard.getId()).thenReturn(i);
            }
            return shardsHolder;
        }
    }

}