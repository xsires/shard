package ru.xsires.shard.postgres;


import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.util.Arrays;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.postgresql.Driver;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import ru.xsires.shard.Abstract3ShardTest;
import ru.yandex.qatools.embed.postgresql.EmbeddedPostgres;
import static ru.yandex.qatools.embed.postgresql.distribution.Version.Main.V9_6;

@Slf4j
@TestConfiguration
public class PostgresqlEmbedded3ShardConfiguration implements BeanFactoryPostProcessor {
    private static final String TEST_DB_NAME = "test-db";
    private final EmbeddedPostgres postgres0 = new EmbeddedPostgres(V9_6);
    private final EmbeddedPostgres postgres1 = new EmbeddedPostgres(V9_6);
    private final EmbeddedPostgres postgres2 = new EmbeddedPostgres(V9_6);

    @Bean(destroyMethod = "stop")
    public EmbeddedPostgres embeddedPostgres0() {
        return postgres0;
    }

    @Bean(destroyMethod = "stop")
    public EmbeddedPostgres embeddedPostgres1() {
        return postgres1;
    }

    @Bean(destroyMethod = "stop")
    public EmbeddedPostgres embeddedPostgres2() {
        return postgres2;
    }


    @SneakyThrows
    private void initPg(EmbeddedPostgres postgres, int port) {
        Path cachedPath = Paths.get(System.getProperty("java.io.tmpdir"));
        try {
            postgres.start(EmbeddedPostgres.cachedRuntimeConfig(cachedPath),
                    "127.0.0.1", port, TEST_DB_NAME, TEST_DB_NAME, TEST_DB_NAME, Arrays.asList(
                            "-E", "UTF-8",
                            "--locale=C",
                            "--lc-collate=C",
                            "--lc-ctype=C"));
            SimpleDriverDataSource simpleDriverDataSource = new SimpleDriverDataSource(new Driver(),
                    "jdbc:postgresql://127.0.0.1:" + port + "/" + TEST_DB_NAME, TEST_DB_NAME, TEST_DB_NAME);
            try (Connection connection = simpleDriverDataSource.getConnection()) {
                ScriptUtils.executeSqlScript(connection, new ClassPathResource("ddl.sql"));
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {
        initPg(postgres0, Integer.parseInt(System.getProperty(Abstract3ShardTest.SHARD_0_PORT_VAR)));
        initPg(postgres1, Integer.parseInt(System.getProperty(Abstract3ShardTest.SHARD_1_PORT_VAR)));
        initPg(postgres2, Integer.parseInt(System.getProperty(Abstract3ShardTest.SHARD_2_PORT_VAR)));
    }


}
