package ru.xsires.shard;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;
import ru.xsires.shard.config.LoadProperty;
import ru.xsires.shard.config.ShardsProperty;
import ru.xsires.shard.model.Identifier;

/**
 * Генерируем данные для работы
 * идентификатор отправителя; идентификатор магазина; сумма операции
 */
@RequiredArgsConstructor
@Service
public class OperationGenerator {
    private static final String SEPARATOR = ";";
    private static final int SHOP_COUNT = 1000;
    private static final int ACCOUNT_COUNT = 100_000;
    private static final int OPERATION_COUNT = 3_000_000;
    private static final int MAX_OPERATION_SUM = 5_000;

    private final LoadProperty loadProperty;
    private final ShardsProperty shardsProperty;

    private void generateOperations(File file) throws IOException {
        file.getParentFile().mkdirs();
        file.createNewFile();
        ThreadLocalRandom random = ThreadLocalRandom.current();

        List<ShardsProperty.ShardProperty> shards = shardsProperty.getItems();
        List<Identifier> shopIds = IntStream
                .range(0, SHOP_COUNT)
                .mapToObj(id -> new Identifier(shards.get(random.nextInt(shards.size())).getId(), id))
                .collect(Collectors.toList());
        List<Identifier> accountIds = IntStream
                .range(0, ACCOUNT_COUNT)
                .mapToObj(id -> new Identifier(shards.get(random.nextInt(shards.size())).getId(), id))
                .collect(Collectors.toList());

        try (FileWriter writer = new FileWriter(file, true)) {
            for (int i = 0; i < OPERATION_COUNT; i++) {
                writer.append(new StringBuilder()
                        .append('"')
                        .append(accountIds.get(random.nextInt(accountIds.size())).asString())
                        .append('"')
                        .append(SEPARATOR)
                        .append('"')
                        .append(shopIds.get(random.nextInt(shopIds.size())).asString())
                        .append('"')
                        .append(SEPARATOR)
                        .append('"')
                        .append(random.nextInt(MAX_OPERATION_SUM) + 1)
                        .append('"')
                        .append("\n")
                        .toString());
            }
        }
    }

    @SneakyThrows
    public void generateOperationsIfNotExists() {
        File file = new File(loadProperty.getDataFilePath());
        if (!file.exists()) {
            generateOperations(file);
        }
    }
}
