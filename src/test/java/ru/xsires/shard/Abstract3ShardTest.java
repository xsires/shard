package ru.xsires.shard;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.SocketUtils;
import ru.xsires.shard.postgres.PostgresqlEmbedded3ShardConfiguration;

@Slf4j
@SpringBootTest
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:3-shard-test.properties")
@Import(PostgresqlEmbedded3ShardConfiguration.class)
public class Abstract3ShardTest {

    public static final String SHARD_0_PORT_VAR = "shard.0.port";
    public static final String SHARD_1_PORT_VAR = "shard.1.port";
    public static final String SHARD_2_PORT_VAR = "shard.2.port";

    static {
        for (int i = 0; i < 3; i++) {
            String port = String.valueOf(SocketUtils.findAvailableTcpPort());
            String shardPortVarName = "shard." + i + ".port";
            log.info("Set: {}={}", shardPortVarName, port);
            System.setProperty(shardPortVarName, port);
        }
    }
}
