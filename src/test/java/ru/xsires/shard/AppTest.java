package ru.xsires.shard;


import java.util.List;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.xsires.shard.dao.ShopPaymentDao;
import ru.xsires.shard.dao.UserPaymentDao;
import ru.xsires.shard.load.LoadService;
import ru.xsires.shard.service.TopPaymentStatistic;


public class AppTest extends Abstract3ShardTest {

    @Autowired
    OperationGenerator operationGenerator;

    @Autowired
    LoadService loadService;

    @Autowired
    TopPaymentStatistic topPaymentStatistic;

    @BeforeEach
    public void initData() {
        operationGenerator.generateOperationsIfNotExists();

    }

    /**
     * Поднимает 3 шарда, загружает тестовые данные, получает top 3
     */
    @Test
    public void example() {
        loadService.loadData();
        {
            List<ShopPaymentDao.ShopPaymentBySum> top3ShopByPaymentSum = topPaymentStatistic.getTop3ShopByPaymentSum();

            Assert.assertThat(top3ShopByPaymentSum, Matchers.hasSize(3));
            Assert.assertThat(top3ShopByPaymentSum.get(0).getAllPaymentsSum(), Matchers.greaterThan(top3ShopByPaymentSum.get(1).getAllPaymentsSum()));
        }

        {
            List<UserPaymentDao.UserPaymentBySum> top3UserByPaymentSum = topPaymentStatistic.getTop3UserByPaymentSum();

            Assert.assertThat(top3UserByPaymentSum, Matchers.hasSize(3));
            Assert.assertThat(top3UserByPaymentSum.get(0).getAllPaymentsSum(), Matchers.greaterThan(top3UserByPaymentSum.get(1).getAllPaymentsSum()));
        }

    }
}
