package ru.xsires.shard.dao;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ru.xsires.shard.engine.Shard;
import ru.xsires.shard.model.Identifier;
import ru.xsires.shard.model.UserPayment;
/**
 * Доступ к данным о тратах пользователя
 */
@Slf4j
@Component
public class UserPaymentDao {

    public void batchInsert(List<UserPayment> payments, Shard shard) {
        log.info("Insert batch: shardId={}, size={}", shard.getId(), payments.size());
        shard.getJdbcTemplate()
                .batchUpdate("INSERT INTO user_payment(id, sum) VALUES (?,?)",
                        payments.stream()
                                .map(payment -> new Object[]{payment.getUserId().asString(), payment.getSum()})
                                .collect(Collectors.toList()));
    }

    public List<UserPaymentBySum> getTopBySum(int count, Shard shard) {
        Object[] args = {count};
        return shard.getJdbcTemplate().query("SELECT id, sum(sum) FROM user_payment GROUP BY id ORDER BY sum(sum) DESC LIMIT ?",
                args,
                (rs, rowNum) -> new UserPaymentBySum(Identifier.parse(rs.getString("id")), rs.getBigDecimal("sum")));
    }

    @Data
    public static class UserPaymentBySum {
        private final Identifier id;
        private final BigDecimal allPaymentsSum;

    }
}

