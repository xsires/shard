package ru.xsires.shard.dao;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ru.xsires.shard.engine.Shard;
import ru.xsires.shard.model.Identifier;
import ru.xsires.shard.model.ShopPayment;

/**
 * Доступ к данным о платежах в магазин
 */
@Slf4j
@Component
public class ShopPaymentDao {

    public void batchInsert(List<ShopPayment> payments, Shard shard) {
        log.info("Insert batch: shardId={}, size={}", shard.getId(), payments.size());
        shard.getJdbcTemplate()
                .batchUpdate("INSERT INTO shop_payment(id, sum) VALUES (?,?)",
                        payments.stream()
                                .map(payment -> new Object[]{payment.getShopId().asString(), payment.getSum()})
                                .collect(Collectors.toList()));
    }

    public List<ShopPaymentBySum> getTopBySum(int count, Shard shard) {
        Object[] args = {count};
        return shard.getJdbcTemplate().query("SELECT id, sum(sum) FROM shop_payment GROUP BY id ORDER BY sum(sum) DESC LIMIT ?",
                args,
                (rs, rowNum) -> new ShopPaymentBySum(Identifier.parse(rs.getString("id")), rs.getBigDecimal("sum")));
    }

    @Data
    public static class ShopPaymentBySum {
        private final Identifier id;
        private final BigDecimal allPaymentsSum;

    }
}
