package ru.xsires.shard.service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.xsires.shard.config.ShardsProperty;
import ru.xsires.shard.dao.ShopPaymentDao;
import ru.xsires.shard.dao.UserPaymentDao;
import ru.xsires.shard.engine.Shard;
import ru.xsires.shard.engine.ShardRouter;

/**
 * Умеет получать статистику по колличеству платежей
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class TopPaymentStatistic {
    private final ShardsProperty shardsProperty;
    private final ShardRouter shardRouter;
    private final UserPaymentDao userPaymentDao;
    private final ShopPaymentDao shopPaymentDao;


    public List<UserPaymentDao.UserPaymentBySum> getTop3UserByPaymentSum() {
        List<Shard> allActiveShards = shardRouter.getAllActiveShards();
        if (allActiveShards.size() != shardsProperty.getItems().size()) {
            log.warn("Not all shards are active");
        }
        List<UserPaymentDao.UserPaymentBySum> result = allActiveShards
                .stream()
                .parallel()
                .flatMap(shard -> userPaymentDao.getTopBySum(3, shard).stream())
                .sorted(Comparator.comparing(UserPaymentDao.UserPaymentBySum::getAllPaymentsSum).reversed())
                .collect(Collectors.toList());
        if (result.size() > 3) {
            return result.subList(0, 3);
        }
        return result;
    }

    public List<ShopPaymentDao.ShopPaymentBySum> getTop3ShopByPaymentSum() {
        List<Shard> allActiveShards = shardRouter.getAllActiveShards();
        if (allActiveShards.size() != shardsProperty.getItems().size()) {
            log.warn("Not all shards are active");
        }
        List<ShopPaymentDao.ShopPaymentBySum> result = allActiveShards
                .stream()
                .parallel()
                .flatMap(shard -> shopPaymentDao.getTopBySum(3, shard).stream())
                .sorted(Comparator.comparing(ShopPaymentDao.ShopPaymentBySum::getAllPaymentsSum).reversed())
                .collect(Collectors.toList());
        if (result.size() > 3) {
            return result.subList(0, 3);
        }
        return result;
    }


}
