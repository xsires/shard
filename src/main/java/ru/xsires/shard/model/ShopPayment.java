package ru.xsires.shard.model;

import java.math.BigDecimal;
import lombok.Data;

/**
 * Платежи в магазин
 */
@Data
public class ShopPayment {
    private final Identifier shopId;
    private final BigDecimal sum;
}
