package ru.xsires.shard.model;

import java.math.BigDecimal;
import lombok.Data;

/**
 * Платеж, пользователем в магазин
 */
@Data
public class Payment {

    /**
     * Идентификатор аккаунта
     */
    private final Identifier accountId;
    /**
     * Идентификатор магазина
     */
    private final Identifier shopId;

    /**
     * Сумма операции
     */
    private final BigDecimal sum;

}
