package ru.xsires.shard.model;

import java.math.BigDecimal;
import lombok.Data;

/**
 * Платежи пользователя
 */
@Data
public class UserPayment {
    private final Identifier userId;
    private final BigDecimal sum;
}
