package ru.xsires.shard.model;

import lombok.Data;

/**
 * Идентификатор сущности, с шардом
 */
@Data
public class Identifier {

    private final byte shardId;
    private final long id;


    public static Identifier parse(String value) {
        String[] split = value.split("-");
        return new Identifier(Byte.valueOf(split[1]), Long.valueOf(split[0]));
    }

    public String asString() {
        return id + "-" + shardId;
    }
}
