package ru.xsires.shard.load;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import javax.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import ru.xsires.shard.config.LoadProperty;
import ru.xsires.shard.config.ShardsProperty;
import ru.xsires.shard.dao.ShopPaymentDao;
import ru.xsires.shard.dao.UserPaymentDao;
import ru.xsires.shard.engine.Shard;
import ru.xsires.shard.engine.ShardRouter;
import ru.xsires.shard.model.Identifier;
import ru.xsires.shard.model.Payment;
import ru.xsires.shard.model.ShopPayment;
import ru.xsires.shard.model.UserPayment;

/**
 * Сервис умеет загружать данные из файла
 */
@Slf4j
@RequiredArgsConstructor
@Service
public class LoadService {

    private final LoadProperty loadProperty;
    private final UserPaymentDao userPaymentDao;
    private final ShopPaymentDao shopPaymentDao;
    private final ShardsProperty shardsProperty;
    private final ShardRouter shardRouter;

    /**
     * Эксклюзивное выполнение {@link #loadData}
     */
    private final Lock loadDataLock = new ReentrantLock();

    private Semaphore semaphore;
    private ThreadPoolExecutor threadPoolExecutor;

    @PostConstruct
    private void init() {
        int threadCount = loadProperty.getThreadCount();
        threadPoolExecutor = new ThreadPoolExecutor(threadCount,
                threadCount,
                0, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>(threadCount),
                new ThreadPoolExecutor.CallerRunsPolicy());

        semaphore = new Semaphore(loadProperty.getThreadCount());
    }


    private Payment parsePayment(String line) {
        // идентификатор отправителя; идентификатор магазина; сумма операции
        String[] split = line.replace("\"", "").split(";");
        return new Payment(Identifier.parse(split[0]), Identifier.parse(split[1]), new BigDecimal(split[2]));
    }

    /**
     * Загружаем тестовые данные в шарды
     */
    @SneakyThrows
    public void loadData() {
        loadDataLock.lock();
        try {
            Map<Byte, List<ShopPayment>> shopPaymentsBatch = new HashMap<>();
            Map<Byte, List<UserPayment>> userPaymentsBatch = new HashMap<>();

            for (ShardsProperty.ShardProperty shardProperty : shardsProperty.getItems()) {
                shopPaymentsBatch.put(shardProperty.getId(), new ArrayList<>(loadProperty.getBatchSize()));
                userPaymentsBatch.put(shardProperty.getId(), new ArrayList<>(loadProperty.getBatchSize()));
            }

            try (BufferedReader br = new BufferedReader(new FileReader(new File(loadProperty.getDataFilePath())))) {
                String line;
                while ((line = br.readLine()) != null) {
                    Payment payment = parsePayment(line);

                    List<UserPayment> userPayments = userPaymentsBatch.get(payment.getAccountId().getShardId());
                    userPayments.add(new UserPayment(payment.getAccountId(), payment.getSum()));
                    if (userPayments.size() == loadProperty.getBatchSize()) {
                        loadUserBatchAsync(userPayments);
                        userPayments.clear();
                    }

                    List<ShopPayment> shopPayments = shopPaymentsBatch.get(payment.getShopId().getShardId());
                    shopPayments.add(new ShopPayment(payment.getShopId(), payment.getSum()));
                    if (shopPayments.size() == loadProperty.getBatchSize()) {
                        loadShopBatchAsync(shopPayments);
                        shopPayments.clear();
                    }
                }
            }
            shopPaymentsBatch
                    .forEach((shardId, payments) -> {
                        if (payments.size() > 0) {
                            loadShopBatchAsync(payments);
                        }
                    });

            userPaymentsBatch
                    .forEach((shardId, payments) -> {
                        if (payments.size() > 0) {
                            loadUserBatchAsync(payments);
                        }
                    });

            while (threadPoolExecutor.getActiveCount() > 0) {
                log.info("Wait batch load: activeLoadCounter={} ", threadPoolExecutor.getActiveCount());
                Thread.sleep(1000);
            }
        } finally {
            loadDataLock.unlock();
        }
    }

    @SneakyThrows
    private void loadUserBatchAsync(List<UserPayment> payments) {
        semaphore.acquire();
        try {
            threadPoolExecutor.execute(new UserBatchLoader(new ArrayList<>(payments)));
        } finally {
            semaphore.release();
        }
    }

    @SneakyThrows
    private void loadShopBatchAsync(List<ShopPayment> payments) {
        semaphore.acquire();
        try {
            threadPoolExecutor.execute(new ShopBatchLoader(new ArrayList<>(payments)));
        } finally {
            semaphore.release();
        }
    }


    private class ShopBatchLoader implements Runnable {
        private final Logger log = LoggerFactory.getLogger(ShopBatchLoader.class);
        private final List<ShopPayment> shopPayments;

        public ShopBatchLoader(List<ShopPayment> shopPayments) {
            this.shopPayments = shopPayments;
        }

        @Override
        public void run() {
            try {
                semaphore.acquire();
                try {
                    try {
                        byte shardId = shopPayments.get(0)
                                .getShopId().getShardId();
                        Shard shard = shardRouter.getShardById(shardId);
                        if (shard == null) {
                            throw new IllegalStateException("Shard is not available id=" + shardId);
                        }
                        shopPaymentDao.batchInsert(shopPayments, shard);
                    } catch (Exception e) {
                        log.error("Load batch fail", e);
                    }


                } finally {
                    semaphore.release();
                }
            } catch (InterruptedException e) {
                log.error("Load batch fail interrupted");
            }
        }
    }


    private class UserBatchLoader implements Runnable {
        private final Logger log = LoggerFactory.getLogger(ShopBatchLoader.class);
        private final List<UserPayment> userPayments;

        public UserBatchLoader(List<UserPayment> userPayments) {
            this.userPayments = userPayments;
        }

        @Override
        public void run() {
            try {
                semaphore.acquire();
                try {
                    try {
                        byte shardId = userPayments.get(0)
                                .getUserId().getShardId();
                        Shard shard = shardRouter.getShardById(shardId);
                        if (shard == null) {
                            throw new IllegalStateException("Shard is not available id=" + shardId);
                        }
                        userPaymentDao.batchInsert(userPayments, shard);
                    } catch (Exception e) {
                        log.error("Load batch fail", e);
                    }

                } finally {
                    semaphore.release();
                }
            } catch (InterruptedException e) {
                log.error("Load batch interrupted");
            }
        }
    }

}
