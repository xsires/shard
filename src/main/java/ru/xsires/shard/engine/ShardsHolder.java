package ru.xsires.shard.engine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import lombok.RequiredArgsConstructor;
import org.apache.commons.dbcp2.DataSourceConnectionFactory;
import org.apache.commons.dbcp2.PoolableConnection;
import org.apache.commons.dbcp2.PoolableConnectionFactory;
import org.apache.commons.dbcp2.PoolingDataSource;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.postgresql.Driver;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.stereotype.Component;
import ru.xsires.shard.config.ShardsProperty;


/**
 * Создает и хранит шарды
 */
@Component
@RequiredArgsConstructor
public class ShardsHolder {

    private final ShardsProperty shardsProperty;
    private List<Shard> shards;

    private DataSource createDataSource(ShardsProperty.ShardProperty shardProperty) {

        SimpleDriverDataSource simpleDriverDataSource = new SimpleDriverDataSource(new Driver(),
                shardProperty.getJdbcUrl(), shardProperty.getUser(), shardProperty.getPassword());

        DataSourceConnectionFactory dataSourceConnectionFactory = new DataSourceConnectionFactory(simpleDriverDataSource);
        GenericObjectPoolConfig<PoolableConnection> config = new GenericObjectPoolConfig<>();
        config.setMaxIdle(shardProperty.getConnectionCount());
        config.setMinIdle(shardProperty.getConnectionCount());

        PoolableConnectionFactory pooledObjectFactory = new PoolableConnectionFactory(dataSourceConnectionFactory, null);
        GenericObjectPool<PoolableConnection> pool = new GenericObjectPool<>(pooledObjectFactory, config);
        pooledObjectFactory.setPool(pool);
        return new PoolingDataSource<>(pool);
    }

    @PostConstruct
    void init() {
        shards = new ArrayList<>(shardsProperty.getItems().size());
        for (ShardsProperty.ShardProperty shardProperty : shardsProperty.getItems()) {
            shards.add(new Shard(createDataSource(shardProperty), shardProperty.getId()));
        }
        this.shards = Collections.unmodifiableList(shards);
    }

    public List<Shard> getShards() {
        return shards;
    }
}
