package ru.xsires.shard.engine.select;

import org.springframework.lang.Nullable;
import ru.xsires.shard.engine.Shard;


public interface ShardObtain {

    @Nullable
    Shard getShardById(byte id);
}
