package ru.xsires.shard.engine.select;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.xsires.shard.config.ShardsProperty;
import ru.xsires.shard.engine.Shard;
import ru.xsires.shard.engine.ShardsHolder;
import ru.xsires.shard.engine.check.HealthCheckResult;
import ru.xsires.shard.engine.check.ShardHealthChecker;
import ru.xsires.shard.engine.resurrection.ShardResurrection;


/**
 * Умеет получать шард по его id, в случае если шард не работает, начнется его востановление
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ShardObtainImpl implements ShardObtain {

    private final ShardsProperty shardsProperty;
    private final ShardsHolder shardsHolder;
    private final ShardHealthChecker shardHealthChecker;
    private final ShardResurrection shardResurrection;


    private List<Shard> getActiveShards() {
        return shardsHolder.getShards().stream()
                .filter(shard -> shard.getShardStatus() == Shard.ShardStatus.ACTIVE)
                .filter(this::checkShard)
                .collect(Collectors.toList());
    }


    /**
     * Проверяем активен ли шард
     *
     * @param shard шард для проверки
     * @return true если шард можно использовать
     */
    private boolean checkShard(Shard shard) {
        if (shard.getShardStatus() != Shard.ShardStatus.ACTIVE) {
            return false;
        }

        if (shardsProperty.getHealthCheckFrequency() > 0 &&
                // FIXME SecureRandom
                ThreadLocalRandom.current().nextInt(shardsProperty.getHealthCheckFrequency()) == 0) {
            HealthCheckResult check = shardHealthChecker.check(shard);
            if (check == HealthCheckResult.FAIL) {
                shardResurrection.resurrect(shard);
                return false;
            }
        }
        return true;
    }

    @Override
    public Shard getShardById(byte id) {
        Optional<Shard> shard = getActiveShards()
                .stream()
                .filter(it -> it.getId() == id)
                .findFirst();

        if (shard.isPresent()) {
            if (checkShard(shard.get())) {
                return shard.get();
            }
        }
        log.error("Shard is not active: id={}", id);
        return null;
    }
}
