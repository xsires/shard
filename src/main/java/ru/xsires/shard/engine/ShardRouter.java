package ru.xsires.shard.engine;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import ru.xsires.shard.config.ShardsProperty;
import ru.xsires.shard.engine.select.ShardObtain;

/**
 * Выбирает очередной шард для работы, доступ к шардам надо получать через этот класс
 */
@Service
@RequiredArgsConstructor
public class ShardRouter {

    private final ShardsProperty shardsProperty;
    private final ShardObtain shardObtain;

    public List<Shard> getAllActiveShards() {
        return shardsProperty.getItems().stream()
                .map(ShardsProperty.ShardProperty::getId)
                .map(shardObtain::getShardById)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    @Nullable
    public Shard getShardById(byte id) {
        return shardObtain.getShardById(id);
    }


}
