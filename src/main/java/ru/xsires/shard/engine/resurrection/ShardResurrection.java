package ru.xsires.shard.engine.resurrection;

import ru.xsires.shard.engine.Shard;

/**
 * Умеет оживлять отказавшие шарды
 */
public interface ShardResurrection {

    /**
     * Переводи шард в фазу "Поломан" и оживляет его
     *
     * @param shard шард который надо оживить
     */
    void resurrect(Shard shard);
}
