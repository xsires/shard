package ru.xsires.shard.engine.resurrection;

import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.xsires.shard.config.ShardsProperty;
import ru.xsires.shard.engine.Shard;
import ru.xsires.shard.engine.ShardsHolder;
import ru.xsires.shard.engine.check.HealthCheckResult;
import ru.xsires.shard.engine.check.ShardHealthChecker;

/**
 * Запускается по треду на шрад, в случае если шард не доуступен,
 * этот тред будет проверять его и вернет в строй через какое-то время
 */
@Service
@RequiredArgsConstructor
public class ShardResurrectionImpl implements ShardResurrection {
    private final ShardsProperty shardsProperty;
    private final ShardsHolder shardsHolder;
    private final ShardHealthChecker checker;

    private Map<Byte, Condition> locksConditions;
    private Map<Byte, Lock> locks;
    private List<Thread> threads;


    @PostConstruct
    void init() {
        locksConditions = new HashMap<>();
        locks = new HashMap<>();
        threads = shardsHolder.getShards()
                .stream()
                .map(shard -> {
                    ReentrantLock lock = new ReentrantLock();
                    Condition condition = lock.newCondition();
                    locks.put(shard.getId(), lock);
                    locksConditions.put(shard.getId(), condition);
                    return new Thread(new ResurrectionThreadImpl(shard,
                            checker,
                            lock,
                            condition,
                            shardsProperty.getResurrectionDelay()));
                })
                .collect(Collectors.toList());
        threads.forEach(thread -> {
            thread.setDaemon(true);
            thread.start();
        });
    }

    @PreDestroy
    void destroy() {
        threads.forEach(Thread::interrupt);
    }

    @Override
    public void resurrect(Shard shard) {
        if (shard.getShardStatus() == Shard.ShardStatus.ACTIVE) {
            Lock lock = locks.get(shard.getId());
            if (lock.tryLock()) {
                try {
                    shard.setShardStatus(Shard.ShardStatus.BROKEN);
                    locksConditions.get(shard.getId()).signal();
                } finally {
                    lock.unlock();
                }
            }
        }
    }


    @Slf4j
    @RequiredArgsConstructor
    private static class ResurrectionThreadImpl implements Runnable {
        private final Shard shard;
        private final ShardHealthChecker checker;
        private final Lock lock;
        private final Condition condition;
        private final Duration sleepTimeBetweenCheck;

        @Override
        public void run() {
            lock.lock();
            try {
                if (await()) {
                    return;
                }
                while (true) {
                    HealthCheckResult result = checker.check(shard);
                    switch (result) {
                        case SUCCESS: {
                            log.info("Shard now active: name={}", shard.getId());
                            shard.setShardStatus(Shard.ShardStatus.ACTIVE);
                            if (await()) {
                                return;
                            }
                            break;
                        }
                        case FAIL: {
                            if (sleepBetween()) {
                                return;
                            }
                            break;
                        }
                        default: {
                            log.error("Unknown: checkResult={}", result);
                            if (sleepBetween()) {
                                return;
                            }
                        }
                    }
                }
            } finally {
                lock.unlock();
            }

        }

        /**
         * Спим между попытками оживить шард
         *
         * @return true если надо заканчивать работу
         */
        private boolean sleepBetween() {
            try {
                Thread.sleep(sleepTimeBetweenCheck.toMillis());
            } catch (InterruptedException e) {
                return true;
            }
            return false;
        }

        /**
         * Ждем пока не сломается шард
         *
         * @return true если надо заканчивать работу
         */
        private boolean await() {
            try {
                condition.await();
            } catch (InterruptedException e) {
                return true;
            }
            return false;
        }
    }
}
