package ru.xsires.shard.engine;

import javax.sql.DataSource;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

/**
 * Шард
 */
@Getter
@EqualsAndHashCode(of = "id")
public class Shard {

    private final byte id;
    private final DataSource dataSource;
    private final JdbcTemplate jdbcTemplate;
    private final TransactionTemplate transactionTemplate;
    @Setter
    volatile ShardStatus shardStatus = ShardStatus.ACTIVE;

    public Shard(DataSource dataSource, byte id) {
        this.dataSource = dataSource;
        this.id = id;
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.transactionTemplate = new TransactionTemplate(new DataSourceTransactionManager(dataSource));
    }

    /**
     * Шард бывают живой или поломанный
     */
    public enum ShardStatus {
        /**
         * Живой шард с котоырм можно работать
         */
        ACTIVE,
        /**
         * Сломанный шард
         */
        BROKEN;
    }

}
