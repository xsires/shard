package ru.xsires.shard.engine.check;

import ru.xsires.shard.engine.Shard;

/**
 * Проверят жив ли шард
 */
public interface ShardHealthChecker {

    HealthCheckResult check(Shard shard);
}
