package ru.xsires.shard.engine.check;

import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.SingleColumnRowMapper;
import org.springframework.stereotype.Service;
import ru.xsires.shard.engine.Shard;

@Slf4j
@Service
public class ShardHealthCheckerImpl implements ShardHealthChecker {

    private final String HEALTH_CHECK_SQL = "SELECT 1";

    @Override
    public HealthCheckResult check(Shard shard) {
        try {
            Integer integer = shard.getJdbcTemplate().queryForObject(HEALTH_CHECK_SQL, new SingleColumnRowMapper<Integer>());
            log.info("Health success: shard={}", shard.getId());
            return HealthCheckResult.SUCCESS;
        } catch (Exception e) {
            // TODO разбирать исключения
            log.info("Health check fail" + shard.getId(), e);
            return HealthCheckResult.FAIL;
        }
    }

}
