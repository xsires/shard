package ru.xsires.shard.engine.check;

/**
 * Результат проверка шарда
 */
public enum HealthCheckResult {
    /**
     * Можно пользоваться
     */
    SUCCESS,
    /**
     * Нельзя пользоваться
     */
    FAIL
}