package ru.xsires.shard.config;


import javax.annotation.PostConstruct;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Настройки инструмента загрузки
 */
@ConfigurationProperties(prefix = "load")
@Configuration
@Data
@Slf4j
public class LoadProperty {

    /**
     * Колличество потоков для загрузки
     */
    private int threadCount;
    /**
     * Путь до файла с данными
     */
    private String dataFilePath;
    /**
     * Размер батча для загрузки
     */
    private int batchSize;

    @PostConstruct
    void init() {
        log.info(this.toString());
    }

}
