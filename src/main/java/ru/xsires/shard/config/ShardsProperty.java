package ru.xsires.shard.config;

import java.time.Duration;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Настройки шардирования
 */
@ConfigurationProperties(prefix = "shards")
@Configuration
@Getter
@Setter
@ToString
@Slf4j
public class ShardsProperty {
    /**
     * Перечень шардов
     */
    private List<ShardProperty> items;

    /**
     * Задержка между попытками оживить шард
     */
    private Duration resurrectionDelay;

    /**
     * Частота проверка жизниспосбности шарда, больше - реже
     */
    private int healthCheckFrequency;

    @PostConstruct
    void init() {
        log.info(this.toString());
        if (items.stream().map(ShardProperty::getId).collect(Collectors.toSet()).size() != items.size()) {
            throw new IllegalArgumentException("Идентификаторы шардов должны быть униклаьны");
        }
    }

    /**
     * Настройки шарда
     */
    @Getter
    @Setter
    @ToString(exclude = "password")
    public static class ShardProperty {
        private String jdbcUrl;
        private String user;
        private String password;
        /**
         * Колличество запуленных соединений
         */
        private int connectionCount;
        /**
         * Идентификтаор шарда
         */
        private byte id;
    }
}
